// package wallet

// import (
// 	"crypto/ecdsa"
// 	"fmt"
// 	"log"

// 	"github.com/ethereum/go-ethereum/common/hexutil"
// 	"github.com/ethereum/go-ethereum/crypto"
// )

// func main() {
//     privateKey, err := crypto.GenerateKey()
//     if err != nil {
//         log.Fatal(err)
//     }

//     privateKeyBytes := crypto.FromECDSA(privateKey)
//     fmt.Println("PK:", hexutil.Encode(privateKeyBytes))

//     publicKey := privateKey.Public()
//     publicKeyECDSA, _ := publicKey.(*ecdsa.PublicKey)

//     publicKeyBytes := crypto.FromECDSAPub(publicKeyECDSA)
//     fmt.Println("PB:", hexutil.Encode(publicKeyBytes)) 

//     address := crypto.PubkeyToAddress(*publicKeyECDSA).Hex()
//     fmt.Println("ADDR:", address)
// }