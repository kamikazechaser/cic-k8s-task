// CGO_ENABLED=0 GOOS=linux go build -a -tags netgo -ldflags '-w' eth-signer.go
package main

import (
	"context"
	"crypto/ecdsa"
	"encoding/json"
	"fmt"
	"log"
	"math/big"
	"net/http"
	"os"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/gorilla/mux"
)

var (
    burnAddress = common.HexToAddress("0x000000000000000000000000000000000000dead")
    myPrivateKey = os.Getenv("PRIVATE_KEY")
    amount = big.NewInt(1000000000000000)
    // Rinkeby
    chainId = big.NewInt(4)
    gas = big.NewInt(150000000000)
    gasLimit = uint64(30000)   
)

func burn(client *ethclient.Client, myPrivateKey string) (string, error) {
	privateKey, err := crypto.HexToECDSA(myPrivateKey)
	if err != nil {
	  log.Print(err)
      return "error", err
	}

	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
    if !ok {
        log.Print("cannot assert type: publicKey is not of type *ecdsa.PublicKey")
        return "error", fmt.Errorf("type assertion failed")
    }

    fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
    nonce, err := client.PendingNonceAt(context.Background(), fromAddress)
    if err != nil {
        log.Print(err)
        return "error", err
    }

    message := []byte(fmt.Sprintf("%s - %s", "lul", time.Now().String()))

    tx := types.NewTransaction(nonce, burnAddress, amount, gasLimit, gas, message)

    signedTx, err := types.SignTx(tx, types.NewEIP155Signer(chainId), privateKey)
    if err != nil {
        log.Print(err)
        return "error", err
    }

    err = client.SendTransaction(context.Background(), signedTx)
    if err != nil {
        log.Print(err)
        return "error", err
    }

    return signedTx.Hash().Hex(), nil
}

func main() {
    client, err := ethclient.Dial("https://rinkeby-light.eth.linkpool.io")
    if err != nil {
        log.Fatal(err)
    }

    router := mux.NewRouter()

	router.HandleFunc("/burn", func(w http.ResponseWriter, r *http.Request) {

        tx, err := burn(client, myPrivateKey)
        
		w.Header().Set("Content-Type", "application/json")
        if err != nil {
            w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]bool{
				"ok": false,
			})
        } else {
            json.NewEncoder(w).Encode(map[string]string{
				"ok": "true",
				"tx_hash": tx,
			})
        }
	})

	srv := &http.Server{
		Handler: router,
		Addr:    ":8000",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Printf("Starting server")
	log.Fatal(srv.ListenAndServe())
}