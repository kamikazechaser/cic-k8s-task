package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func main() {
	store := make(map[string]string)
	store["Bearer xyz"] = "user 1"
	store["Bearer abc"] = "user 2"

	router := mux.NewRouter()

	router.HandleFunc("/check", func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authorization")

		w.Header().Set("Content-Type", "application/json")
		if user, found := store[token]; found {
			log.Printf("Authenticated user %s\n", user)
			w.Header().Set("X-Auth-User", user)
			json.NewEncoder(w).Encode(map[string]string{
				"ok": "true",
				"user": user,
			})
        } else {
        	log.Printf("Auth failed")
			w.WriteHeader(http.StatusUnauthorized)
			json.NewEncoder(w).Encode(map[string]bool{
				"ok": false,
			})
        }	
	})

	srv := &http.Server{
		Handler: router,
		Addr:    ":8000",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Printf("Starting server")
	log.Fatal(srv.ListenAndServe())
}