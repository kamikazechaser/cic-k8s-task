## Cmd

```bash
# terraform/
$ source .env

# terraform/k8s-cluster
$ terrafrom init
$ terraform plan
$ terraform apply

# Wait for the DO LoadBalancer IP to be provisioned
# Update A records:
#   whoamisvc.domain
#   vault.domain
# Update domain names in the manifests below

# manifests/
$ kubectl apply -f cert_manager.yml
$ kubectl apply -f whoami_service.yml
$ kubectl apply -f vault_service.yml
$ kubectl create serviceaccount vault-auth
$ kubectl apply -f vault_service_account.yml

# Initialize and unseal Vault uing the Web UI

# terraform/vault
$ terrafrom init
$ terraform plan
$ terraform apply

# manifests/
$ kubectl apply -f external_secrets.yml
$ kubectl apply -f burner.yml
```

#### Known issues

1. Cert provisioning fails if DNS records aren't set before requesting for a new cert. See [issue#1](https://github.com/jetstack/cert-manager/issues/3008) and [issue#2](https://stackoverflow.com/questions/62390107/kubernetes-challenge-waiting-for-http-01-propagation-dial-tcp-no-such-host).
2. Vault UI isn't available to initialize the vault or unseal it when running in HA (High Availability) mode. See [issue#1](https://github.com/hashicorp/vault-helm/issues/326).

### Vault

Running in standalone mode and [perists data to a PVC](https://www.vaultproject.io/docs/configuration/storage/filesystem) inside the k8s cluster. UI is exposed at https://vault.domain. Uses [Kubernetes service account authentication](https://www.vaultproject.io/docs/auth/kubernetes) to pull secrets from Vault. Uses [external-secrets](https://external-secrets.io/) operator to sync Vault secrets with k8s secrets.

A sample `eth-signer` microservice demonstrates creating and signing transactions by using a private key stored in Vault.

### Traefik

Uses [IngressRoute](https://doc.traefik.io/traefik/providers/kubernetes-crd/) to define routing and middleware.

Send a request:

```
> GET / HTTP
> Host: whoamisvc.domain
> authorization: Bearer xyz or Bearer abc
> accept: */*
```

You should receive a response with:

```
Hostname: whoami-* (One of the whomami pods)
X-Auth-User: user 1 or user 2
```

Other tokens should be rejected
