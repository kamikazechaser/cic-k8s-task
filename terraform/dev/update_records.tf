terraform {
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 2.0"
    }
  }
}

variable "cf_email" {}
variable "cf_api_key" {}
variable "cf_zone_id" {}
variable "do_ip" {}

provider "cloudflare" { 
  email   = var.cf_email
  api_key = var.cf_api_key
}

resource "cloudflare_record" "whoami" {
  zone_id = "${var.cf_zone_id}"
  name    = "whoamisvc"
  value   = "${var.do_ip}"
  type    = "A"
  proxied = false
}

resource "cloudflare_record" "vault" {
  zone_id = "${var.cf_zone_id}"
  name    = "vault"
  value   = "${var.do_ip}"
  type    = "A"
  proxied = false
}

resource "cloudflare_record" "burner" {
  zone_id = "${var.cf_zone_id}"
  name    = "burner"
  value   = "${var.do_ip}"
  type    = "A"
  proxied = false
}