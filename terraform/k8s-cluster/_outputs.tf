output "k8s_endpoint" {
  value = digitalocean_kubernetes_cluster.cic_k8s_testing.endpoint
}

output "k8s_token" {
  sensitive = true
  value     = digitalocean_kubernetes_cluster.cic_k8s_testing.kube_config[0].token
}

output "k8s_cert" {
  sensitive = true
  value     = digitalocean_kubernetes_cluster.cic_k8s_testing.kube_config[0].cluster_ca_certificate
}