resource "helm_release" "traefik" {
  depends_on    = [digitalocean_kubernetes_cluster.cic_k8s_testing]
  chart         = "traefik"
  force_update  = true
  repository    = "https://helm.traefik.io/traefik"
  name          = "traefik"
  recreate_pods = true
  values        = [file("traefik_values.yml")]
}

resource "helm_release" "cert_manager" {
  depends_on    = [helm_release.traefik]
  chart         = "cert-manager"
  force_update  = true
  repository    = "https://charts.jetstack.io"
  name          = "cert-manager"
  recreate_pods = true

  set {
    name  = "installCRDs"
    value = "true"
  }  
}

resource "helm_release" "vault" {
  depends_on    = [helm_release.cert_manager]
  chart         = "vault"
  force_update  = true
  repository    = "https://helm.releases.hashicorp.com"
  name          = "vault"
  recreate_pods = true
  values        = [file("vault_values.yml")]
}

resource "helm_release" "external_secrets" {
  depends_on    = [helm_release.cert_manager]
  chart         = "external-secrets"
  force_update  = true
  repository    = "https://charts.external-secrets.io"
  name          = "external-secrets"
  recreate_pods = true
}
