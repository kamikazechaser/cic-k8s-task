provider "digitalocean" {
  token = var.do_token
}

provider "helm" {
  kubernetes {
    host                   = digitalocean_kubernetes_cluster.cic_k8s_testing.endpoint
    token                  = digitalocean_kubernetes_cluster.cic_k8s_testing.kube_config[0].token
    cluster_ca_certificate = base64decode(
      digitalocean_kubernetes_cluster.cic_k8s_testing.kube_config[0].cluster_ca_certificate
    )
  }
}