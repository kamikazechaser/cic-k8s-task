resource "digitalocean_kubernetes_cluster" "cic_k8s_testing" {
  name    = "cic-k8s-testing"
  region  = "blr1"
  version = "1.21.2-do.2"

  node_pool {
    name       = "autoscaling-worker-pool"
    size       = "s-1vcpu-2gb"
    auto_scale = true
    min_nodes  = 4
    max_nodes  = 6
  }
}
