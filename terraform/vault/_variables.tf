variable "k8s_host" {}
variable "k8s_token" {}
variable "k8s_cert" {}

variable "vault_endpoint" {}
variable "vault_token" {}
