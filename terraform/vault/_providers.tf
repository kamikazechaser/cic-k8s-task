provider "kubernetes" {
  host                   = var.k8s_host
  token                  = var.k8s_token
  cluster_ca_certificate = base64decode(var.k8s_cert)
}

provider "vault" {
  address = var.vault_endpoint
  token   = var.vault_token
}