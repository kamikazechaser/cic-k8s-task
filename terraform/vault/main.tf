terraform {
  required_providers {
    vault = {
      source = "hashicorp/vault"
      version = "2.23.0"
    }

    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.4.1"
    }
  }
}