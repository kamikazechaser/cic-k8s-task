resource "vault_auth_backend" "kubernetes" {
  type = "kubernetes"
}

data "kubernetes_service_account" "vault_auth" {
  metadata {
    name = "vault-auth"
  }
}

data "kubernetes_secret" "vault_auth" {
  metadata {
    name = "${data.kubernetes_service_account.vault_auth.default_secret_name}"
  }
}

resource "vault_kubernetes_auth_backend_config" "vault_kubernetes_auth" {
  kubernetes_ca_cert     = base64decode(var.k8s_cert)
  backend                = vault_auth_backend.kubernetes.path
  kubernetes_host        = "https://kubernetes:443"
  token_reviewer_jwt     = "${data.kubernetes_secret.vault_auth.data.token}"
  disable_iss_validation = "true"
  disable_local_ca_jwt   = "true"
}

resource "vault_policy" "microservices_policy" {
  name = "microservices"
  policy = file("policies.hcl")
}

resource "vault_kubernetes_auth_backend_role" "example" {
  depends_on                       = [vault_policy.microservices_policy]
  backend                          = vault_auth_backend.kubernetes.path
  role_name                        = "apps"
  bound_service_account_names      = ["vault-auth"]
  bound_service_account_namespaces = ["default"]
  token_ttl                        = 0
  token_policies                   = ["microservices", "default"]
}